"""
Helpers for integrating bamboo in analysis workflows (git and workflow management tools) and improving reproducibility
"""
import dataclasses
import enum
from functools import partial
import logging 
logger = logging.getLogger(__name__)
import os.path
import shutil
import subprocess
import urllib.parse
from . import utils as utils

def _callGit(args, cwd=None, stderr=None):
    return subprocess.check_output(["git"]+args, cwd=cwd, stderr=stderr).decode().strip()

hasGit = shutil.which("git") is not None
if not hasGit:
    logger.error("No git executable found")
else:
    gitVersionStr = subprocess.check_output(["git", "--version"]).decode().strip()
    logger.debug(f"Found GIT: {gitVersionStr}")
    import packaging.version
    gitVersion = packaging.version.parse(gitVersionStr.split()[2])
    if gitVersion < packaging.version.parse("2.5"):
        logger.warning("The git version is below 2.5, please consider installing a more recent version to take advantage of worktrees")

class NotGitTrackedException(Exception):
    def __init__(self, path, message):
        self.path = path
        super().__init__(message)

def splitGitWorkdir(filePath):
    """ Find the top-level directory, relative path, and .git directory for a file """
    filePath = utils.labspath(os.path.expanduser(filePath))
    fDir = os.path.dirname(utils.labspath(filePath))
    if not hasGit:
        raise NotGitTrackedException(filePath, f"Cannot check git status of {filePath} (no git executable found)")
    try:
        cdUp = _callGit(["rev-parse", "--show-cdup"], cwd=fDir, stderr=subprocess.DEVNULL)
        topLevel = os.path.normpath(os.path.join(fDir, cdUp))
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 128:
            raise NotGitTrackedException(filePath, f"File {filePath} is not in a git repository") from None
        raise
    fileRelPath = os.path.relpath(filePath, topLevel)
    logger.debug(f"Repository for file {filePath}: {topLevel}, relative path: {fileRelPath}")
    return topLevel, fileRelPath

def getRemotes(workdir):
    remotes = _callGit(["remote"], cwd=workdir).split()
    remInfo = {}
    for remName in remotes:
        try:
            remInfo[remName] = {
                    "url": _callGit(["remote", "get-url", remName], cwd=workdir, stderr=subprocess.DEVNULL),
                    "url_push": _callGit(["remote", "get-url", "--push", remName], cwd=workdir, stderr=subprocess.DEVNULL)
                    }
        except subprocess.CalledProcessError as ex:  # git below 2.7
            remInfo[remName] = {
                    "url": _callGit(["config", "--get", f"remote.{remName}.url"], cwd=workdir)
                    }
    return remInfo

@dataclasses.dataclass
class GitVersionInfo:
    is_dirty: bool = False
    version: str = ""
    sha1: str = ""
    git_common: str = ""
    remotes: dict = dataclasses.field(default_factory=dict)
    tag: str = ""
    tag_remotes: list = dataclasses.field(default_factory=list)
    remote_branches: list = dataclasses.field(default_factory=list)
    untracked_files: list = dataclasses.field(default_factory=list)
    def asDict(self):
        return dataclasses.asdict(self)
    class Status(enum.IntEnum):
        DIRTY = 0
        LOCALCOMMIT = 1
        COMMIT = 2
        LOCALTAG = 3
        TAG = 4
    @property
    def status(self):
        if self.is_dirty:
            return GitVersionInfo.Status.DIRTY
        elif self.version == self.tag:
            if self.tag_remotes:
                return GitVersionInfo.Status.TAG
            else:
                return GitVersionInfo.Status.LOCALTAG
        else:
            if self.remote_branches:
                return GitVersionInfo.Status.COMMIT
            else:
                return GitVersionInfo.Status.LOCALCOMMIT
    POLICY_MIN = {
        "testing": Status.DIRTY,
        "committed": Status.LOCALCOMMIT,
        "tagged": Status.LOCALTAG,
        "pushed": Status.TAG,
        }
    @staticmethod
    def forDir(workdir, withRemote=True):
        callGit = partial(_callGit, cwd=workdir)
        inst = GitVersionInfo()
        inst.sha1 = callGit(["rev-parse", "--verify", "--short", "HEAD"])
        inst.version = callGit(["describe", "--tags", "--always", "--dirty"])
        if "-dirty" in inst.version:
            inst.is_dirty = True
        logger.debug(f"Version: {inst.version}, short hash: {inst.sha1}")
        gitCommon = callGit(["rev-parse", "--git-common-dir"])
        if gitCommon == "--git-common-dir":  # git below 2.5
            gitCommon = os.path.join(_callGit(["rev-parse", "--show-toplevel"], cwd=workdir, stderr=subprocess.DEVNULL), ".git")
        if not os.path.isabs(gitCommon):
            gitCommon = os.path.normpath(os.path.join(workdir, gitCommon))
        elif os.path.realpath(workdir) != workdir:  # symlinks in workdir, use them
            pWD, pGC = utils.realcommonpath(workdir, gitCommon)
            gitCommon = os.path.join(pWD, os.path.relpath(gitCommon, pGC))
        inst.git_common = gitCommon
        if withRemote:
            inst.remotes = {remNm: remInfo for remNm, remInfo in getRemotes(workdir).items()
                    if urllib.parse.urlparse(remInfo["url"]).scheme}  # exclude local clones
            tags = callGit(["tag", "-l"]).split()
            tag = inst.version.split("-")[0]
            if tag in tags:  # set and find on remotes
                inst.tag = tag
                for remNm in inst.remotes:
                    try:
                        lst = callGit(["ls-remote", "--tags", remNm, tag], stderr=subprocess.DEVNULL)
                        if lst:
                            remHash = lst.split()[0]
                            locHash = callGit(["rev-parse", tag])
                            if remHash != locHash:
                                logger.error(f"Tag {tag} has a different hash on remote ({remHash} vs {locHash})! Please fix")
                            else:
                                inst.tag_remotes.append(remNm)
                    except:
                        logger.error(f"Could not check if tag {tag} is on remote {remNm}")
                if inst.tag_remotes:
                    logger.debug(f"Tag {tag} found in remote(s): {', '.join(inst.tag_remotes)}")
            # search for the commit on remote
            inst.remote_branches = [remBr.strip() for remBr in callGit(["branch", "--remotes", "--no-color", "--contains", inst.sha1]).split()]
            if inst.remote_branches:
                logger.debug(f"Commit {inst.sha1} found in remote branches: {', '.join(inst.remote_branches)}")
            # check for untracked
            untracked = callGit(["ls-files", "--others", "--exclude-standard"]).split()
            if untracked:
                if len(untracked) > 10:
                    logger.warning(f"{len(untracked):d} untracked files found in {workdir}")
                else:
                    logger.warning(f"Untracked files found in {workdir}: {', '.join(untracked)}")
                inst.untracked_files = list(untracked)
        return inst

    def check(self, what="", policy="pushed"):
        if what:
            what = f" for {what}"
        if self.status == GitVersionInfo.Status.DIRTY:
            logger.error(f"Your working tree{what} is dirty! Please commit, tag, and push for reproducible results")
        elif self.status == GitVersionInfo.Status.LOCALCOMMIT:
            logger.warning(f"Please tag and/or push commit {self.sha1}{what} for results that are reproducible beyond your local repository")
        elif self.status == GitVersionInfo.Status.COMMIT:
            logger.warning(f"Running with commit {self.sha1}{what}. Please tag (and push) for better traceability")
        elif self.status == GitVersionInfo.Status.LOCALTAG:
            logger.warning(f"Running with tag {self.tag}{what}. Please push for better traceability")
        elif self.status == GitVersionInfo.Status.TAG:
            logger.info(f"Running with tag {self.tag}{what}")
        if self.status < GitVersionInfo.POLICY_MIN[policy]:
            raise RuntimeError(f"Git status {self.status.name}{what} failed git policy '{policy}'")
