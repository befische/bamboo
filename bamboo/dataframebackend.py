"""
ROOT::RDataFrame backend classes
"""
import bamboo.logging
logger = bamboo.logging.getLogger(__name__)

from itertools import chain
from functools import partial
import numpy as np
from timeit import default_timer as timer
import resource

from .plots import FactoryBackend, Selection, SelectionWithSub, Plot, DerivedPlot, Skim
from . import treefunctions as op
from . import treeoperations as top

from collections import defaultdict
_RDFNodeStats = defaultdict(int)
_RDFHistoNDStats = defaultdict(int)
_RDFHistoND_methods = dict()
## _RDFHistoND_methods[kyTypes] = getattr(gbl.rdfhelpers.rdfhistofactory, f"Histo{nVars:d}D")[tuple(templTypes)]
def _printStats():
    logger.info("Number of uses per node type: {0}".format(
        ", ".join("{1:d} {0}".format(ndTp, num)
        for ndTp, num in _RDFNodeStats.items())))
    if _RDFHistoNDStats:
        logger.info("HistoND calls per column type: {0}".format(
            ", ".join("{1:d} {0} -> {2};{3}".format(kyTypes[0], num, ",".join(kyTypes[1]), (kyTypes[2] if len(kyTypes) > 2 else ""))
            for kyTypes, num in _RDFHistoNDStats.items())))
    if _RDFHistoND_methods:
        logger.debug("HistoND helper instantiations: {0}".format(
            ", ".join("{0}::Histo{2:d}D<{1}>".format(kyTypes[0], ",".join(list(kyTypes[1])+([kyTypes[2]] if len(kyTypes) > 2 else [])), len(kyTypes[1]))
            for kyTypes in _RDFHistoND_methods.keys())))
def _resetStats():
    global _RDFNodeStats
    global _RDFHistoNDStats
    global _RDFHistoND_methods
    _RDFNodeStats = defaultdict(int)
    _RDFHistoNDStats = defaultdict(int)
    _RDFHistoND_methods = dict()

class FilterWithDefines(top.CppStrRedir):
    """ Branching point in the RDF graph: a Filter and the Define nodes attached to it """
    def __init__(self, parent, variation="nominal"):
        self.explDefine = list()
        self.var = None # nodes for related systematic variations (if different by more than the weight)
        self.children = dict()
        if isinstance(parent, FilterWithDefines):
            self.parent = parent
            self.backend = parent.backend
            self.df = parent.df
            self._definedColumns = dict(parent._definedColumns)
            if variation == "nominal":
                self.var = dict((varNm, FilterWithDefines(pvar, variation=varNm)) for varNm, pvar in parent.var.items())
                # parent is either nominal (when branching off), or the corresponding variation of the parent of the nominal node
        elif isinstance(parent, DataframeBackend):
            self.parent = None
            self.backend = parent
            self.df = parent.rootDF
            self._definedColumns = dict()
            self.var = dict()
        else:
            raise RuntimeError("Can only define FilterWithDefines with a DataframeBackend or another FilterWithDefines")
        top.CppStrRedir.__init__(self)

    def childWithCuts(self, cuts):
        cutExpr = Selection._makeExprAnd(cuts)
        if cutExpr not in self.children:
            ## trick: by doing this *before* constructing the nominal node, any definitions
            ## end up in the node above, and are in principle available for other sub-selections too
            cutStr = cutExpr.get_cppStr(defCache=self)
            newNd = FilterWithDefines(self) ## branching the RDataFrame graph here
            newNd.df = self.backend.df_filter(newNd.df, cutStr, cutExpr, newNd)
            self.children[cutExpr] = newNd
            logger.debug(f"Created child FilterWithDefines for {cutExpr.get_cppStr(defCache=self)}")
        else:
            logger.debug(f"Re-using child FilterWithDefines for {cutExpr.get_cppStr(defCache=self)}")
        return self.children[cutExpr]

    def _getColName(self, op):
        if op in self._definedColumns:
            return self._definedColumns[op]
    stop = _getColName

    def define(self, expr):
        """ explicitly define column for expression (returns the column name) """
        if not self._getColName(expr):
            self.explDefine.append(expr)
        return self(expr)

    def _define(self, name, expr):
        if expr in self._definedColumns:
            cppStr = self(expr) ## corner case: define with a new name
        else:
            cppStr = expr.get_cppStr(defCache=self)
        self.df = self.backend.df_define(self.df, name, cppStr, expr, self)
        self._definedColumns[expr] = name

    def symbol(self, decl, **kwargs):
        return self.backend.symbol(decl, **kwargs)

    def _inExplDefines(self, arg):
        return arg in self.explDefine or ( self.parent and self.parent._inExplDefines(arg) )

    def shouldDefine(self, arg):
        return self.backend.shouldDefine(arg, defCache=self) or self._inExplDefines(arg)

    def __call__(self, arg):
        """ Get the C++ string corresponding to an op """
        nm = self._getColName(arg)
        if ( not self.shouldDefine(arg) ) and not nm: ## the easy case
            try:
                return arg.get_cppStr(defCache=self)
            except Exception as ex:
                logger.exception("Could not get cpp string for {0!r}: {1!r}", arg, ex)
                return "NONE"
        else:
            if not nm: ## define it then
                nm = self.backend.getUColName()
                self._define(nm, arg)
            return nm

    def makeWeight(self, weights, wName=None, parentWeight=None):
        if parentWeight is None and len(weights) == 1 and ( isinstance(weights[0], top.GetColumn) or self._getColName(weights[0]) ):
            if isinstance(weights[0], top.GetColumn):
                weight = (weights[0].name, weights[0].typeName)
            else:
                weight = (self._getColName(weights[0]), weights[0].typeName)
        else:
            weightExpr = Selection._makeExprProduct(
                ([top.GetColumn(parentWeight[1], parentWeight[0]).result]+weights) if parentWeight
                else weights
                )
            if self._getColName(weightExpr):
                wName = self._getColName(weightExpr)
            else:
                self._define(wName, weightExpr)
            weight = (wName, weightExpr.typeName)
        return weight

class SelectionHelper:
    # for lack of a better name... will keep the weight(s) for a selection and
    # the FilterWithDefines ref (can be the same for different Selections and SelectionHelpers)
    def __init__(self, fdnd):
        self.fdnd = fdnd # FilterWithDefines-node reference
        self.weight = dict()

## NOTE these are global (for the current process&interpreter)
## otherwise they would be overwritten in sequential mode (this even allows reuse)
_gSymbols = dict()
_giFun = 0

def _normFunArgs(expr, args, argNames):
    newExpr = expr
    newArgs = args
    for i,argN in sorted(list(enumerate(argNames)), key=(lambda elm : len(elm[1])), reverse=True):
        newName = "myArg{0:d}".format(i, argN)
        if " " in argN:  # parameter-like, with a comment
            if sum(1 for ia in newArgs if argN in ia) != 1: ## should be in one and only one argument
                raise RuntimeError("{0} is in more than one (or none) of {1}".format(argN, newArgs))
            newArgs = [ (ia.replace(argN, newName) if argN in ia else ia) for ia in newArgs ]
        else:  # just a column name
            newArgs = [ (" ".join((newName if tk == argN else tk) for tk in ia.split())
                        if argN in ia else ia) for ia in newArgs ]
        newExpr = newExpr.replace(argN, newName)
    return newExpr, newArgs

def histoName(plot, variation="nominal"):
    name = plot.name
    if variation != "nominal":
        name = "__".join((name, variation))
    return name

def _adjustBinnings(binnings):
    # high-dimensional histo models require either all or none of the binnings to be uniform
    if len(binnings) > 1:
        from .plots import EquidistantBinning, VariableBinning
        if any((isinstance(b, VariableBinning) for b in binnings)):
            import numpy as np
            binnings = [VariableBinning(np.linspace(b.minimum, b.maximum, b.N + 1)) if isinstance(b, EquidistantBinning) else b for b in binnings]
    return binnings

def defineAndGetVarNames(nd, variables, weight=None, uName=None):
    varNamesTypes = []
    for i,var in enumerate(variables):
        if isinstance(var, top.GetColumn):
            varNamesTypes.append((var.name, var.typeName))
        elif isinstance(var, top.ForwardingOp) and isinstance(var.wrapped, top.GetColumn):
            varNamesTypes.append((var.wrapped.name, var.wrapped.typeName))
        else:
            if nd._getColName(var):
                varNm = nd._getColName(var)
            else:
                nm = f"v{i:d}_{uName}"
                nd._define(nm, var)
                varNm =nm
            varNamesTypes.append((varNm, var.typeName))
    if weight is not None:
        varNamesTypes.append(weight)
    return varNamesTypes

class SkimTreeHandle:
    def __init__(self, rresult, fileName, treeName):
        self.rresult = rresult
        self.fileName = fileName
        self.treeName = treeName

import bamboo.plots
class CFRCounter(bamboo.plots.Product):
    def __init__(self, name, title=None):
        super(CFRCounter, self).__init__(name)
        self.variables = (None,)
        self.binnings = (bamboo.plots.EquidistantBinning(1, 0., 1.),)
        self.title = title if title else name

class DataframeBackend(FactoryBackend):
    def __init__(self, tree):
        from .root import gbl
        self.rootDF = gbl.ROOT.RDataFrame(tree).Define("_zero_for_stats", "0")
        self.selDFs = dict()      ## (selection name, variation) -> SelWithDefines
        self.results = dict()     ## product name -> list of result pointers
        self.allSysts = set()     ## all systematic uncertainties and variations impacting any plot
        self._cfrMemo = defaultdict(dict)
        super(DataframeBackend, self).__init__()
        self._iCol = 0
    def _getUSymbName(self):
        global _giFun
        _giFun += 1
        return "myFun{0:d}".format(_giFun)
    def getUColName(self):
        self._iCol += 1
        return "myCol{0:d}".format(self._iCol)
    def shouldDefine(self, op, defCache=None):
        return op.canDefine and ( isinstance(op, top.RangeOp) or isinstance(op, top.DefineOnFirstUse) )
    def define(self, op, selection):
        self.selDFs[selection.name].fdnd.define(op)

    def symbol(self, decl, resultType=None, args=None, nameHint=None):
        isFunction = (resultType and args)
        if isFunction: ## then it needs to be wrapped in a function
            decl = "{result} <<name>>({args})\n{{\n  return {0};\n}};\n".format(
                        decl, result=resultType, args=args)
        global _gSymbols
        if decl in _gSymbols:
            return _gSymbols[decl]
        else:
            if nameHint and nameHint not in _gSymbols.values():
                name = nameHint
            else:
                name = self._getUSymbName()
            _gSymbols[decl] = name
            self.int_declare(decl.replace("<<name>>", name), isFunction=isFunction)
            return name

    def makeHelperFunction(self, cppStr, expr, defCache, asLambda=False):
        depList = top._collectDeps(expr, defCache=defCache)
        _, paramDecl, paramNames, paramCall = top._convertFunArgs(depList, defCache=defCache)
        expr_n, paramDecl_n = _normFunArgs(cppStr, paramDecl, paramNames)
        if defCache._getColName(expr) or any(isinstance(expr, tp) for tp in (top.GetColumn, top.GetArrayColumn)):
            _, paramDecl, paramNames, paramCall = top._convertFunArgs([expr], defCache=defCache)
            expr_n, paramDecl_n = _normFunArgs(cppStr, paramDecl, paramNames)
            funDecl = "[] ( {0} ) {{ return {1}; }}".format(
                    ", ".join(paramDecl_n), expr_n)
            return funDecl, paramCall
        elif not asLambda:
            funName = defCache.symbol(expr_n, resultType=expr.typeName, args=", ".join(paramDecl_n))
            return funName, paramCall
        else:
            funDecl = "[] ( {0} ) {{ return {1}; }}".format(
                    ", ".join(paramDecl_n), expr_n)
            return funDecl, paramCall

    ## wrappers for gInterpreter::Declare, RDF::Define and RDF::Filter
    def int_declare(self, code, isFunction=False):
        logger.debug("Defining new symbol with interpreter: {0}", code)
        from .root import gbl
        gbl.gInterpreter.Declare(code)
        _RDFNodeStats["gInterpreter_Declare"] += 1
    def df_define(self, df, name, cppStr, expr, defCache):
        if isinstance(expr, top.RangeOp) and not defCache._getColName(expr):
            funName, param = self.makeHelperFunction(cppStr, expr, defCache)
            cppStr = f"{funName}({', '.join(param)})"
        logger.debug("Defining {0} as {1}", name, cppStr)
        _RDFNodeStats["Define"] += 1
        return df.Define(name, cppStr)
    def df_filter(self, df, cppStr, expr, defCache):
        if isinstance(expr, top.RangeOp) and not defCache._getColName(expr):
            funName, param = self.makeHelperFunction(cppStr, expr, defCache)
            cppStr = f"{funName}({', '.join(param)})"
        logger.debug("Filtering with {0}", cppStr)
        _RDFNodeStats["Filter"] += 1
        return df.Filter(cppStr)
    def df_origColNames(self):
        return [ str(cN) for cN in self.rootDF.GetColumnNames() if str(cN) != "_zero_for_stats" ]
    def df_snapshot(self, df, treeName, outputFileName, colNToKeep, maxEvents=-1):
        if maxEvents > 0:
            df = df.Range(maxEvents)
        from .root import gbl
        snapOpts = gbl.ROOT.RDF.RSnapshotOptions()
        snapOpts.fLazy = True
        _RDFNodeStats["Snapshot"] += 1
        return df.Snapshot(treeName, outputFileName, colNToKeep, snapOpts)

    @classmethod
    def create(cls, decoTree, nThreads=None):
        if nThreads:
            from .root import gbl
            logger.info(f"Enabling implicit MT for {nThreads:d} threads")
            gbl.ROOT.EnableImplicitMT(nThreads)
        inst = cls(decoTree._tree)
        rootSel = Selection(inst, "none")
        return inst, rootSel

    def addSelection(self, sele):
        """ Define ROOT::RDataFrame objects needed for this selection """
        if sele.name in self.selDFs:
            raise RuntimeError(f"A Selection with name '{sele.name}' already exists")
        nomParentNd, parentHelper = None, None
        if sele.parent is None: ## root no-op selection
            assert not sele._cuts ## this one cannot have cuts
            nomNd = FilterWithDefines(self)
            helper = SelectionHelper(nomNd)
        else:
            parentHelper = self.selDFs[sele.parent.name]
            nomParentNd = parentHelper.fdnd
            if sele._cuts:
                nomNd = nomParentNd.childWithCuts(sele._cuts)
            else:
                nomNd = nomParentNd
            helper = SelectionHelper(nomNd)
        nomParWeight = None
        if parentHelper and parentHelper.weight:
        #if sele.parent is not None and sele.parent.weight:
            nomParWeight = parentHelper.weight["nominal"]
        if sele._weights:
            helper.weight["nominal"] = nomNd.makeWeight(
                    sele._weights,
                    wName=f"w_{sele.name}",
                    parentWeight=nomParWeight
                    )
        elif nomParWeight:
            helper.weight["nominal"] = nomParWeight
        self.selDFs[sele.name] = helper

        if sele.autoSyst:
            ## construct variation nodes (if necessary)
            seleSysts = sele.systematics
            logger.debug("Adding systematic variations {0}", ", ".join(seleSysts))
            for varn in seleSysts:  # all (cut and weight) variations
                ## _cSysts and _wSysts are { variation : { cut/weight : [ nodes to change ] } }
                ## add cuts to the appropriate node, if affected by systematics (here or up)
                varParentNd = None ## set parent node if not the nominal one
                if nomParentNd and varn in nomParentNd.var: ## -> continue on branch
                    logger.debug("{0} systematic variation {1}: continue on branch", sele.name, varn)
                    varParentNd = nomParentNd.var[varn]
                elif varn in sele._cSysts: ## -> branch off now
                    logger.debug("{0} systematic variation {1}: create branch", sele.name, varn)
                    varParentNd = nomParentNd
                if not varParentNd: ## cuts unaffected (here and in parent), can stick with nominal
                    varNd = nomNd
                else: ## on branch, so add cuts (if any)
                    if len(sele._cuts) == 0: ## no cuts, reuse parent
                        varNd = varParentNd
                    else:
                        ctMod = []
                        for ct in sele._cuts:
                            if ct in sele._cSysts[varn]:
                                clNds = list()
                                newct = ct.clone(select=sele._cSysts[varn][ct].__contains__, selClones=clNds)
                                assert len(clNds) >= len(sele._cSysts[varn][ct]) ## should clone all
                                for nd in clNds:
                                    nd.changeVariation(varn)
                                ctMod.append(newct)
                            else:
                                ctMod.append(ct)
                        varNd = varParentNd.childWithCuts(ctMod)
                    nomNd.var[varn] = varNd
                ## next: attach weights (modified if needed) to varNd
                parw = parentHelper.weight.get(varn, parentHelper.weight.get("nominal"))
                if not sele._weights:
                    logger.debug("{0} systematic variation {1}: reusing {2}", sele.name, varn, (parw[0] if parw is not None else "none"))
                    helper.weight[varn] = parw
                else:
                    if varn in sele._wSysts or varNd != nomNd or ( parentHelper and varn in parentHelper.weight ):
                        wfMod = []
                        for wf in sele._weights:
                            if wf in sele._wSysts[varn]:
                                clNds = list()
                                newf = wf.clone(select=sele._wSysts[varn][wf].__contains__, selClones=clNds)
                                assert len(clNds) >= len(sele._wSysts[varn][wf]) ## should clone all
                                for nd in clNds:
                                    nd.changeVariation(varn)
                                wfMod.append(newf)
                            else:
                                wfMod.append(wf)
                        logger.debug("{0} systematic variation {1}: defining new weight based on {2}", sele.name, varn, parw)
                        helper.weight[varn] = varNd.makeWeight(wfMod, wName=(f"w_{sele.name}__{varn}" if sele._weights else None), parentWeight=parw)
                    else: ## varNd == nomNd, not branched, and parent does not have weight variation
                        logger.debug("{0} systematic variation {1}: reusing nominal {2}", sele.name, varn, (varNd.weight["nominal"][0] if varNd.weight["nominal"] is not None else "none"))
                        helper.weight[varn] = helper.weight["nominal"]

    def addPlot(self, plot, autoSyst=True):
        """ Define ROOT::RDataFrame objects needed for this plot (and keep track of the result pointer) """
        if plot.key in self.results:
            raise ValueError(f"A Plot with key '{plot.key}' has already been added")

        selHelper = self.selDFs[plot.selection.name]
        nomNd = selHelper.fdnd
        plotRes = []
        ## Add nominal plot
        nomWeight = selHelper.weight.get("nominal")
        if plot._weights:
            logger.debug("Plot {0}: defining new nominal weight based on {1}",
                    plot.name, (nomWeight[0] if nomWeight is not None else "none"))
            nomWeight = nomNd.makeWeight(plot._weights, parentWeight=nomWeight,
                    wName=f"w_{plot.selection.name}_{plot.name}")
        nomVarNamesTypes = defineAndGetVarNames(nomNd, plot.variables, weight=nomWeight, uName=plot.name)
        plotRes.append(self.makeHistoND(nomNd, nomVarNamesTypes, plot=plot, plotName=plot.name))

        if plot.selection.autoSyst and autoSyst:
            ## Same for all the systematics
            varSysts = top.collectSystVars(plot.variables)
            pwSysts  = top.collectSystVars(plot._weights)
            ## plotSysts, selSysts and allSysts are just {varName}, without the nodes to change
            plotSysts = set(varSysts)
            plotSysts.update(pwSysts)
            selSysts = plot.selection.systematics
            allSysts = set(selSysts)
            allSysts.update(plotSysts)
            self.allSysts.update(allSysts)
            for varn in allSysts:
                if varn in varSysts or varn in nomNd.var:
                    varNd = nomNd.var.get(varn, nomNd)
                    varMod = []
                    for xVar in plot.variables:
                        if xVar in varSysts[varn]:
                            clNds = list()
                            newVar = xVar.clone(select=varSysts[varn][xVar].__contains__, selClones=clNds)
                            assert len(clNds) >= len(varSysts[varn][xVar]) ## should clone all
                            for nd in clNds:
                                nd.changeVariation(varn)
                            varMod.append(newVar)
                        else:
                            varMod.append(xVar)
                    varNamesTypes = defineAndGetVarNames(varNd, varMod, uName=f"{plot.name}__{varn}")
                else: ## can reuse variables, but need to take care of weight
                    varNd = nomNd
                    varNamesTypes = nomVarNamesTypes[:len(plot.variables)]
                weight = selHelper.weight[varn if varn in selSysts and varn in selHelper.weight else "nominal"]
                if plot._weights:
                    wfMod = []
                    if varn in pwSysts:
                        ## plot weights have variation, vary
                        for wf in plot._weights:
                            if wf in pwSysts[varn]:
                                clNds = list()
                                newf = wf.clone(select=pwSysts[varn][wf].__contains__, selClones=clNds)
                                assert len(clNds) >= len(pwSysts[varn][wf]) ## should clone all
                                for nd in clNds:
                                    nd.changeVariation(varn)
                                wfMod.append(newf)
                            else:
                                wfMod.append(wf)
                    elif varn in selSysts and varn in selHelper.weight:
                        ## variation in selection weight
                        wfMod = plot._weights
                    if wfMod: ## need a weight for this variation
                        logger.debug("Plot {0} systematic variation {1}: defining new weight based on {2}",
                                plot.name, varn, (weight[0] if weight is not None else "none"))
                        weight = varNd.makeWeight(wfMod, parentWeight=weight,
                                wName=f"w_{plot.selection.name}_{plot.name}__{varn}")
                    else: ## no systematic that affects the weight
                        weight = nomWeight
                if weight is not None:
                    varNamesTypes.append(weight)
                plotRes.append(self.makeHistoND(varNd, varNamesTypes,
                    plot=plot, variation=varn, plotName=f"{plot.name} variation {varn}"))

        self.results[plot.key] = plotRes

    @classmethod
    def makePlotModel(cls, plot, hName):
        from .root import gbl
        modCls = getattr(gbl.ROOT.RDF, "TH{0:d}DModel".format(len(plot.binnings)))
        return modCls(hName, plot.title, *chain.from_iterable(
            cls.makeBinArgs(binning) for binning in _adjustBinnings(plot.binnings)))
    @classmethod
    def makeBinArgs(cls, binning):
        from .plots import EquidistantBinning, VariableBinning
        if isinstance(binning, EquidistantBinning):
            return (binning.N, binning.mn, binning.mx)
        elif isinstance(binning, VariableBinning):
            return (binning.N, np.array(binning.binEdges, dtype=np.float64))
        else:
            raise ValueError("Binning of unsupported type: {0!r}".format(binning))

    def makeHistoND(self, nd, allVars, plot=None, variation="nominal", plotName=None):
        plotModel = self.__class__.makePlotModel(plot, histoName(plot, variation=variation))
        nAx = len(plot.variables)
        axTypes = [ nd.df.GetColumnType(vNm) for vNm,_ in allVars ]
        ## TODO check & warn with allVars[:,1]
        useExplicit = True
        from .root import gbl
        for vN,vT in allVars:
            try:
                tp = getattr(gbl, vT)
                if hasattr(tp, "value_type"):
                    useExplicit = False
            except AttributeError:
                pass
        if len(allVars) != nAx: ## nontrivial weight
            logger.debug("Adding plot {0} with variables {1} and weight {2}",
                    plotName, ", ".join(vNm for vNm,_ in allVars[:-1]), allVars[-1][0])
        else:
            logger.debug("Adding plot {0} with variables {1}",
                    plotName, ", ".join(vNm for vNm,_ in allVars))
        if useExplicit and nAx < 3: ## only have templates for those
            ndCppName = nd.df.__class__.__cpp_name__
            if nAx != len(axTypes):
                kyTypes = (ndCppName, tuple(axTypes[:-1]), axTypes[-1])
            else:
                kyTypes = (ndCppName, tuple(axTypes))
            _RDFHistoNDStats[kyTypes] += 1
            if kyTypes not in _RDFHistoND_methods:
                templTypes = [ndCppName] + axTypes
                logger.debug(f"Declaring Histo{nAx:d}D helper for types {templTypes}")
                _RDFHistoND_methods[kyTypes] = getattr(gbl.rdfhelpers.rdfhistofactory, f"Histo{nAx:d}D")[tuple(templTypes)]
            plotFun = partial(_RDFHistoND_methods[kyTypes], nd.df)
        else:
            logger.debug(f"Using Histo{nAx:d}D with type inference")
            plotFun = getattr(nd.df, f"Histo{nAx:d}D")
        _RDFNodeStats[f"Histo{nAx:d}D"] += 1
        return plotFun(plotModel, *(vNm for vNm,_ in allVars))

    def getResults(self, plot, key=None):
        return plot.produceResults(self.results.get((key if key is not None else plot.key), []), self, key=key)

    def addSkim(self, skim):
        if skim.key in self.results:
            raise ValueError(f"A Skim with key '{skim.key}' has already been added")

        selHelper = self.selDFs[skim.selection.name]
        selND = selHelper.fdnd

        colNToKeep = list()
        origcolN = self.df_origColNames()
        defcolN = list(selND._definedColumns.values())
        if hasattr(selND.df, "GetDefinedColumnNames"): ## only for non-compiled
            s_df = set(str(nm) for nm in selND.df.GetDefinedColumnNames() if str(nm) != "_zero_for_stats")
            dfnotba = s_df - set(defcolN)
            if dfnotba:
                logger.error(f"In dataframe but not bamboo: {', '.join(dfnotba)}")
                raise RuntimeError("Mismatch in defined columns, see above")
        for toKeep in skim.originalBranches:
            if toKeep is Skim.KeepAll:  # keep all if not defined
                for cN in origcolN:
                    if cN not in colNToKeep:
                        colNToKeep.append(cN)
            elif isinstance(toKeep, Skim.KeepRegex):
                import re
                pat = re.compile(toKeep.pattern)
                for cN in origcolN:
                    if pat.match(cN) and cN not in colNToKeep:
                        colNToKeep.append(cN)
            elif isinstance(toKeep, str):
                if toKeep not in origcolN:
                    raise RuntimeError("Requested column '{0}' from input not found".format(toKeep))
                if toKeep in defcolN:
                    raise RuntimeError("Requested column '{0}' from input is a defined column".format(toKeep))
                if toKeep not in colNToKeep:
                    colNToKeep.append(toKeep)
            else:
                raise RuntimeError("Cannot interpret {0!r} as column list to keep".format(toKeep))

        for dN, dExpr in skim.definedBranches.items():
            if dN in origcolN:
                logger.warning(f"Requested to add column {dN} with expression, but a column with the same name on the input tree exists. The latter will be copied instead")
            elif dN not in defcolN:
                selND._define(dN, top.adaptArg(dExpr))
            if dN not in colNToKeep:
                colNToKeep.append(dN)

        from .root import gbl
        colNToKeep_v = getattr(gbl, "std::vector<std::string>")()
        for cn in colNToKeep:
            colNToKeep_v.push_back(cn)
        rres = self.df_snapshot(selND.df, skim.treeName, skim.outFile, colNToKeep_v, maxEvents=skim.maxSelected)
        self.results[skim.key] = [SkimTreeHandle(rres, skim.outFile, skim.name)]

    def addCutFlowReport(self, report, selections, key=None, autoSyst=True):
        logger.debug("Adding cutflow report {0} for selection(s) {1}".format(report.name, ", ".join(sele.name for sele in selections.values())))
        cmArgs = {"autoSyst" : autoSyst, "makeEntry" : report.__class__.Entry, "prefix" : report.name}
        memo = self._cfrMemo[report.name]
        results = []
        for name, sele in selections.items():
            if sele.name in memo:
                cfr = memo[sele.name]
            else:
                cfr = self._makeCutFlowReport(sele, name, **cmArgs)
                memo[sele.name] = cfr
            results.append(cfr)
        if key is None:
            key = report.name
        self.results[key] = self.results.get(key, []) + results

    def _makeCutFlowReport(self, selection, name, autoSyst=True, makeEntry=None, prefix=None):
        from .root import gbl
        selH = self.selDFs[selection.name]
        selND = selH.fdnd
        nomWeight = selH.weight.get("nominal")
        nomName = f"{prefix}_{name}"
        zVar = [("_zero_for_stats", "int")]
        cfrNom = self.makeHistoND(selND, zVar+([nomWeight] if nomWeight is not None else []),
                plotName=nomName, plot=CFRCounter(nomName,
                    f"CutFlowReport {prefix} nominal counter for {selection.name}"))
        cfrSys = {}
        if autoSyst:
            for varNm in selection.systematics:
                if varNm in selND.var or selH.weight[varNm][0] != nomWeight[0]:
                    cfrSys[varNm] = self.makeHistoND(selND.var.get(varNm, selND),
                            zVar+[selH.weight[varNm]], plotName=nomName,
                            plot=CFRCounter(f"{nomName}__{varNm}",
                                f"CutFlowReport {prefix} {varNm} counter for {selection.name}"))
        return makeEntry(name, cfrNom, cfrSys)

    def buildGraph(self, plotList):
        ## nothing to be done in this case, all already added before
        _printStats()
        _resetStats()

    def addDependency(self, **kwargs):
        from .root import loadDependency
        return loadDependency(**kwargs)
    def getHandle(self, name):
        from .root import gbl
        return SimpleHandle(getattr(gbl, name))

class LazyDataframeBackend(DataframeBackend):
    """
    An experiment: a FactoryBackend implementation that instantiates nodes late (when buildGraph is called)
    """
    def __init__(self, tree):
        super(LazyDataframeBackend, self).__init__(tree)
        self.selections = dict()
        self.definesPerSelection = dict()
        self.plotsPerSelection = dict()
        self.skimsPerSelection = dict()
        self.cutFlowReports = []
        self._definedSel = set()
    def addSelection(self, selection):
        ## keep track and do nothing
        if selection.name in self.selections:
            raise RuntimeError(f"A Selection with name '{selection.name}' already exists")
        self.selections[selection.name] = selection
        self.definesPerSelection[selection.name] = []
        self.plotsPerSelection[selection.name] = []
        self.skimsPerSelection[selection.name] = []
    def addPlot(self, plot, autoSyst=True):
        ## keep track and do nothing
        if any((ap.key == plot.key) for selPlots in self.plotsPerSelection.values() for (ap, aSyst) in selPlots):
            raise RuntimeError(f"A Plot with key '{plot.key}' already exists")
        self.plotsPerSelection[plot.selection.name].append((plot, autoSyst))
    def addSkim(self, skim):
        if any((ap.key == skim.key) for selSkims in self.skimsPerSelection.values() for ap in selSkims):
            raise RuntimeError(f"A Skim with key '{skim.key}' already exists")
        self.skimsPerSelection[skim.selection.name].append(skim)
    def addCutFlowReport(self, report, selections, key=None, autoSyst=True):
        self.cutFlowReports.append((report, selections, key, autoSyst))
    def _buildSelGraph(self, selName, plotList):
        sele = self.selections[selName]
        if sele.parent and sele.parent.name not in self._definedSel:
            self._buildSelGraph(sele.parent.name, plotList)
        super(LazyDataframeBackend, self).addSelection(sele)
        for op in self.definesPerSelection[selName]:
            super(LazyDataframeBackend, self).define(op, sele)
        for plot, autoSyst in self.plotsPerSelection[selName]:
            if plot in plotList or (isinstance(plot.key, tuple) and any(ap.name == plot.key[0] for ap in plotList)):
                super(LazyDataframeBackend, self).addPlot(plot, autoSyst=autoSyst)
        for skim in self.skimsPerSelection[selName]:
            if skim in plotList or (isinstance(skim.key, tuple) and any(ap.name == skim.key[0] for ap in plotList)):
                super(LazyDataframeBackend, self).addSkim(skim)
        self._definedSel.add(selName)
    def buildGraph(self, plotList):
        ## this is the extra method: do all the addSelection/addPlot/addDerivedPlot calls in a better order
        logger.info("Starting to build RDataFrame graph")
        ## collect all plots
        start = timer()
        def getDeps_r(plot):
            if isinstance(plot, DerivedPlot):
                for dp in plot.dependencies:
                    yield dp
                    yield from getDeps_r(dp)
        allPlots = list(plotList) + list(chain.from_iterable(getDeps_r(p) for p in plotList))
        for plot in allPlots:
            if isinstance(plot, Plot) or isinstance(plot, Skim):
                if plot.selection.name not in self._definedSel:
                    self._buildSelGraph(plot.selection.name, allPlots)
                if isinstance(plot.selection, SelectionWithSub):
                    for subSel in plot.selection.sub.values():
                        if subSel.name not in self._definedSel:
                            self._buildSelGraph(subSel.name, allPlots)
        for report, selections, key, autoSyst in self.cutFlowReports:
            super(LazyDataframeBackend, self).addCutFlowReport(report, selections, key=key, autoSyst=autoSyst)
        end = timer()
        ## performance monitoring printout
        maxrssmb = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024
        logger.info(f"RDataFrame graph built in {end - start:.2f}s, max RSS: {maxrssmb:.2f}MB")
        _printStats()
        _resetStats()
    def define(self, op, selection):
        self.definesPerSelection[selection.name].append(op)

import os.path
import pkg_resources
import subprocess
import tempfile
import shutil
from . import root
class FullyCompiledBackend(LazyDataframeBackend):
    """ Another experiment: compile a standalone executable to avoid any JITting in RDF """
    skeleton_cmake = pkg_resources.resource_filename("bamboo", os.path.join("data", "CMakeLists.txt"))
    skeleton_cpp = pkg_resources.resource_filename("bamboo", os.path.join("data", "main.cc"))
    def __del__(self):
        if self.cleanupTmp and self.tmpDir and os.path.isdir(self.tmpDir) and self.tmpDir.startswith(tempfile.gettempdir()):
            logger.debug(f"Cleaning up temporary directory {self.tmpDir}")
            shutil.rmtree(self.tmpDir)
        else:
            logger.info(f"Leaving all generated code and cmake outputs in {self.tmpDir}")
    _genCode = {
            # cmake
            "findpackages" : [],
            "bamboocomponents": list(root._defaultBambooLibs),
            "libdependencies": list(root._defaultDependencies)+list(root._defaultBambooLibs),
            "includedirs" : [],
            "linkdirs" : [],
            # cc
            "includes": [f'#include "hdr"' for hdr in root._defaultHeaders],
            "helpermethods" : [],
            "initialisation" : [],
            }
    gen_entriesFmt = {
            "includes" : (list(root._defaultHeaders), lambda hdrs : [f'#include "{hdr}"' for hdr in hdrs]),
            "includedirs" : ([], lambda dirs : [
                "target_include_directories(bambooExecutor PRIVATE {0})".format(" ".join(
                    f"$<BUILD_INTERFACE:{idr}>" for idr in dirs))]),
            "linkdirs" : ([], lambda dirs : [
                "target_link_directories(bambooExecutor PUBLIC {0})".format(" ".join(dirs))])
            }
    @staticmethod
    def _addGenEntries(key, entries):
        if key in FullyCompiledBackend.gen_entriesFmt:
            gEnt, fmt = FullyCompiledBackend.gen_entriesFmt[key]
            gEnt.extend(entries)
            FullyCompiledBackend._genCode[key] = fmt(gEnt)
        else:
            FullyCompiledBackend._genCode[key] += entries

    def __init__(self, tree, nThreads=None):
        super(FullyCompiledBackend, self).__init__(tree)
        self.tTree = tree
        self.rootDF = "df0"
        self._iDF = 0
        self.nThreads = (nThreads if nThreads else 1)
        self.cmakeConfigOptions = ["-DCMAKE_BUILD_TYPE=Release"]
        self.cmakeBuildOptions = []
        self.cleanupTmp = True
        self.tmpDir = tempfile.mkdtemp()
        logger.debug(f"Temporary directory for compilation: {self.tmpDir}")
        self.tmpOutF = None
        self.resultDefinitions = ['auto df0 = df.Define("_zero_for_stats", []() { return 0; }, {});']
        self.inputFileLists = None
    @property
    def genCode(self):
        gc = dict(FullyCompiledBackend._genCode)
        gc["resultdefinitions"] = self.resultDefinitions
        return gc
    def getUDFName(self):
        self._iDF += 1
        return f"df{self._iDF:d}"

    def shouldDefine(self, op, defCache=None):
        return op.canDefine and ( isinstance(op, top.RangeOp) or isinstance(op, top.DefineOnFirstUse) or isinstance(op, top.Parameter) )

    @classmethod
    def create(cls, decoTree, nThreads=None):
        inst = cls(decoTree._tree, nThreads=nThreads)
        rootSel = Selection(inst, "none")
        return inst, rootSel

    def int_declare(self, code, isFunction=False):
        if not isFunction: ## no need to have helper functions dynamically
            super(FullyCompiledBackend, self).int_declare(code, isFunction=isFunction)
        FullyCompiledBackend._genCode["helpermethods"].append(code)
    def df_define(self, df, name, cppStr, expr, defCache):
        funNameOrDef, param = self.makeHelperFunction(cppStr, expr, defCache,
                asLambda=(not isinstance(expr, top.RangeOp)))
        newdf = self.getUDFName()
        self.resultDefinitions.append(
                f'auto {newdf} = {df}.Define("{name}", {funNameOrDef}, '+
                "{{ {0} }});".format(", ".join('"{0}"'.format(p) for p in param)))
        _RDFNodeStats["Define"] += 1
        return newdf
    def df_filter(self, df, cppStr, expr, defCache):
        funNameOrDef, param = self.makeHelperFunction(cppStr, expr, defCache,
                asLambda=(( not isinstance(expr, top.RangeOp) ) or defCache._getColName(expr)))
        newdf = self.getUDFName()
        self.resultDefinitions.append(
                f"auto {newdf} = {df}.Filter({funNameOrDef},"+
                "{{ {0} }});".format(", ".join('"{0}"'.format(p) for p in param)))
        _RDFNodeStats["Filter"] += 1
        return newdf
    def df_origColNames(self):
        from .root import gbl
        return list(str(cN) for cN in gbl.ROOT.RDataFrame(self.tTree).GetColumnNames())
    def df_snapshot(self, df, treeName, outputFileName, colNToKeep, maxEvents=-1):
        if maxEvents > 0:
            newdf = self.getUDFName()
            self.resultDefinitions.append(f"auto {newdf} = {df}.Range({maxEvents:d});")
            df = newdf
        colNamesStr = ", ".join(f'"{cN}"' for cN in colNToKeep)
        self.resultDefinitions.append(
                f'{df}.Snapshot("{treeName}", "{os.path.abspath(outputFileName)}", {{ {colNamesStr} }});')
        self.buildGraph([])
        self._run(outputFileName)

    def makeHistoND(self, nd, allVars, plot=None, variation="nominal", plotName=None):
        hName = histoName(plot, variation=variation)
        self.resultDefinitions.append(
                f"results.push_back({nd.df}.Histo{len(plot.variables):d}D"+
                "<{0}>({1}, {2}));".format(
                    ", ".join(vTp for _,vTp in allVars),
                    self.__class__.makePlotModel(plot, hName),
                    ", ".join(f'"{vNm}"' for vNm,_ in allVars)))
        _RDFNodeStats[f"Histo{len(plot.variables):d}D"] += 1
        return hName
    @classmethod
    def makePlotModel(cls, plot, hName):
        binArgs = ", ".join(cls.makeBinArgs(bn) for bn in _adjustBinnings(plot.binnings))
        return f'ROOT::RDF::TH{len(plot.binnings):d}DModel("{hName}", "{plot.title}", {binArgs})'
    @classmethod
    def makeBinArgs(cls, binning):
        from .plots import EquidistantBinning, VariableBinning
        if isinstance(binning, EquidistantBinning):
            return f"{binning.N:d}, {binning.mn:f}, {binning.mx:f}"
        elif isinstance(binning, VariableBinning):
            return "{0:d}, {{ {1} }}".format(len(binning.N),
                    ", ".join(str(be) for be in binning.binEdges))
        else:
            raise ValueError(f"Binning of unsupported type: {binning!r}")
    def _writeWithSubstitution(self, skeletonFile, placeholder):
        with open(skeletonFile) as skF:
            srcLines = []
            for skLn in skF:
                if skLn.lstrip().startswith(placeholder):
                    ind,ky = skLn.split(placeholder)
                    srcLines += [ f"{ind}{ln}" for ln in self.genCode[ky.strip()] ]
                else:
                    srcLines.append(skLn.rstrip("\n"))
        with open(os.path.join(self.tmpDir, os.path.basename(skeletonFile)), "w") as srcF:
            fullSrc = "\n".join(srcLines)
            logger.debug("Full source: \n{0}", fullSrc)
            srcF.write(fullSrc)
    def buildGraph(self, plotList):
        ## parent buildGraph method will generate all code through the methods above
        super(FullyCompiledBackend, self).buildGraph(plotList)
        self._writeWithSubstitution(FullyCompiledBackend.skeleton_cmake, "# BAMBOO_INSERT")
        self._writeWithSubstitution(FullyCompiledBackend.skeleton_cpp, "// BAMBOO_INSERT")
        ## Now cross fingers...
        logger.info("Compiling executable")
        start = timer()
        try:
            configCmd = ["cmake"]+self.cmakeConfigOptions+[
                f'-Dbamboo_DIR={pkg_resources.resource_filename("bamboo", "cmake")}',
                f'-DCMAKE_MODULE_PATH={pkg_resources.resource_filename("bamboo", "data")}',
                self.tmpDir]
            out = subprocess.check_output(configCmd, cwd=self.tmpDir, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as ex:
            self.cleanupTmp = False
            logger.error("Problem while running cmake, full output of '{0}':\n{1}", " ".join(configCmd), ex.output.decode().strip())
            raise ex
        else:
            logger.debug("Full output of '{0}:\n{1}", " ".join(configCmd), out.decode().strip())
        try:
            buildCmd = ["cmake", "--build", self.tmpDir]+self.cmakeBuildOptions
            out = subprocess.check_output(buildCmd, cwd=self.tmpDir, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as ex:
            self.cleanupTmp = False
            logger.error("Problem while building, full output of '{0}':\n{1}", " ".join(buildCmd), ex.output.decode().strip())
            raise ex
        else:
            logger.debug("Full output of '{0}':\n{1}", " ".join(buildCmd), out.decode().strip())
        end = timer()
        logger.info(f"Executable compilation finished in {end - start:.2f}s")
    def _run(self, outFileName):
        inFileArr = self.tTree.GetListOfFiles() ## assumes TChain
        inFileNames = [ inFileArr.At(i).GetTitle() for i in range(inFileArr.GetEntries()) ]
        threadsMsg = (f" ({self.nThreads:d} threads)" if self.nThreads > 1 else "")
        logger.info(f"Processing {len(inFileNames):d} files with standalone executable{threadsMsg}")
        try:
            cmd = [os.path.join(self.tmpDir, "bambooExecutor"),
                f"--output={outFileName}",
                f"--tree={self.tTree.GetName()}",
                f"--threads={self.nThreads:d}"
                ]+([f"--input-files={inFN}" for inFN in self.inputFileLists]
                    if self.inputFileLists is not None else
                    [f"--input={inFN}" for inFN in inFileNames])
            logger.info(f"Full command: {' '.join(cmd)}")
            subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as ex:
            self.cleanupTmp = False
            logger.error("Problem while running standalone executable, full output:\n{0}", ex.output.decode())
            raise ex
    def _loadResultsFromTmp(self, tmpOutF):
        loadedRes = dict()
        for pNm, pResults in self.results.items():
            loadedRes[pNm] = [ (self.tmpOutF.Get(res) if isinstance(res, str)
                else res._load(self.tmpOutF)) for res in pResults ]
        self.results = loadedRes
    def getResults(self, plot, key=None):
        if not self.tmpOutF:
            tmpOutFileName = os.path.join(self.tmpDir, "out.root")
            self._run(tmpOutFileName)
            ## then load them in the results dictionary to make the res work
            from .root import gbl, returnToDirectory
            with returnToDirectory():
                # keep a ref to the file because it owns the histograms
                # (and use to detect this part has run)
                self.tmpOutF = gbl.TFile.Open(tmpOutFileName)
                self._loadResultsFromTmp(self.tmpOutF)
        return super(FullyCompiledBackend, self).getResults(plot, key=key)

    def addDependency(self, bambooLib=None, includePath=None, headers=None,
            dynamicPath=None, libraries=None, package=None, components=None):
        ## start with loading them as usual
        super(FullyCompiledBackend, self).addDependency(bambooLib=bambooLib,
                includePath=includePath, headers=headers,
                dynamicPath=dynamicPath, libraries=libraries)
        def asList(arg):
            if isinstance(arg, str):
                return [arg]
            else:
                return list(arg)
        if includePath:
            FullyCompiledBackend._addGenEntries("includedirs", asList(includePath))
        if headers:
            FullyCompiledBackend._addGenEntries("includes", asList(headers))
        if dynamicPath:
            FullyCompiledBackend._addGenEntries("linkdirs", asList(dynamicPath))
        if libraries:
            FullyCompiledBackend._genCode["libdependencies"] += asList(libraries)
        if bambooLib:
            FullyCompiledBackend._genCode["bamboocomponents"] += asList(bambooLib)
            FullyCompiledBackend._genCode["libdependencies"] += asList(bambooLib)
        if package:
            FullyCompiledBackend._genCode["findpackages"].append("find_package({0} REQUIRED{1})".format(
                package, ("" if not components else
                    " COMPONENTS {0}".format(", ".join(asList(components))))))
    def getHandle(self, name):
        raw = super(FullyCompiledBackend, self).getHandle(name)
        return TracingConfigHandle(raw, name, FullyCompiledBackend._genCode["initialisation"])

class SimpleHandle:
    ## for dynamic configuration (converting TupleOp to PyROOT objects)
    class Method:
        def __init__(self, this, name):
            self.this = this
            self.name = name
        def __call__(self, *args):
            return self.this._methodCall(self.name, *args)
    def __init__(self, raw):
        self.raw = raw
    def __getattr__(self, name):
        if hasattr(self.raw, name):
            return SimpleHandle.Method(self, name)
    def _methodCall(self, name, *args):
        args = tuple(self._convert(arg) for arg in args)
        return getattr(self.raw, name)(*args)
    def _convert(self, arg):
        if isinstance(arg, top.TupleBaseProxy):
            arg = top.adaptArg(arg)
        if isinstance(arg, top.TupleOp):
            from .root import gbl
            return arg.getPyROOT(gbl)
        else: ## leave it to PyROOT
            return arg

class TracingConfigHandle(SimpleHandle):
    ## add tracing for compiled
    def __init__(self, raw, name, collector):
        super(TracingConfigHandle, self).__init__(raw)
        self.name = name
        self.collector = collector
    def _convertArg(self, arg):
        if isinstance(arg, int):
            return "{0:d}".format(arg)
        elif isinstance(arg, float):
            return "{0:f}".format(arg)
        elif isinstance(arg, str):
            return '"{0}"'.format(arg)
        elif arg.__class__.__name__.startswith("array<"):
            return "std::{0}{{{{{1}}}}}".format(arg.__class__.__name__, ", ".join(str(arg[i]) for i in range(arg.size())))
        elif isinstance(arg, top.TupleBaseProxy) or isinstance(arg, top.TupleOp):
            if isinstance(arg, top.TupleBaseProxy):
                arg = top.adaptArg(arg)
            return arg.get_cppStr(defCache=top.cppNoRedir)
        else:
            raise RuntimeError(f"Cannot convert {arg!r} yet")
    def _methodCall(self, name, *args):
        fun = getattr(self.raw, name)
        res = fun(*args)
        ln = f"{self.name}.{name}({', '.join(self._convertArg(arg) for arg in args)});"
        logger.debug("Adding init line: {0}".format(ln.replace("{", "{{").replace("}", "}}")))
        self.collector.append(ln)
        return res

def getBackend(decoTree, name=None, nThreads=None):
    if name == "lazy":
        cls = LazyDataframeBackend
    elif name == "compiled":
        cls = FullyCompiledBackend
    else:
        cls = DataframeBackend
    return cls.create(decoTree, nThreads=nThreads)
